FROM edenhill/kcat:1.7.1

RUN apk add --no-cache docker jq httpie bash curl

RUN curl https://raw.githubusercontent.com/birdayz/kaf/master/godownloader.sh | BINDIR=/usr/bin bash
RUN curl https://raw.githubusercontent.com/coursehero/slacktee/master/slacktee.sh > /usr/local/bin/slacktee
RUN chmod +x /usr/local/bin/slacktee
RUN curl https://raw.githubusercontent.com/rockymadden/slack-cli/master/src/slack > /usr/local/bin/slack
RUN chmod +x /usr/local/bin/slack

COPY ./process-payments /usr/bin/process-payments
COPY ./tenants.txt /etc/tenants.txt

ENTRYPOINT [ "/usr/bin/process-payments" ]
